import sys
import os
import webbrowser

from flask import Blueprint, render_template, request, flash, session, redirect, url_for, Response
import requests
import json
from app.src.models.LKODUser import LKODUser
from app.src.models.LoginForm import LoginForm
from app.src.models.MigrationForm import MigrationForm
from app.src.models.Migrator import MIGRATION_STEP_LIMIT, Migrator
from app.src.components.clm.DatasetStatus import DatasetStatus
from flask_wtf import FlaskForm
import urllib.parse


template_path = 'site/'

site = Blueprint('site', __name__)


@site.route('/', methods=['GET', 'POST'])
def index():
    form = LoginForm()
    migration_form = MigrationForm()
    if request.method == 'POST' and form.login_form_submit.data:
        user = LKODUser('')
        user.logout()
        if form.validate_on_submit() and form.process_data() is not False:
            return render_template(template_path + 'index.html', form=form, migration_form=migration_form, dataset_status=DatasetStatus)
    if request.method == 'POST' and migration_form.migration_form_submit.data and migration_form.validate_on_submit():
        if migration_form.process_data():
            return redirect(url_for('site.migrate'))

    return render_template(template_path + 'index.html', form=form, dataset_status=DatasetStatus)


@site.route('/migrate/', methods=['GET'])
@site.route('/migrate/<int:transaction_id>', methods=['GET'])
def migrate(transaction_id:int = 0):
    lkod_configuration = session['lkod']
    ckan_configuration = session['ckan']
    organization_configuration = session['user']
    configuration = session['settings']
    datasets = session['datasets']
    migration_status = session['migration'] if 'migration' in session else None
    migrator = Migrator(lkod_configuration['url'], ckan_configuration['url'], ckan_configuration['api_key'], organization_configuration['vatin'], configuration['variant'], datasets)
    transaction_id = transaction_id + 1
    if migration_status is None:
        migrator.migrate(configuration, limit=MIGRATION_STEP_LIMIT)
        return redirect(url_for('site.migrate', transaction_id=transaction_id))
    elif migration_status and migration_status['last_dataset'] is not None:
        migrator.migrate(configuration, migration_status['last_dataset'], limit=MIGRATION_STEP_LIMIT)
        return redirect(url_for('site.migrate', transaction_id=transaction_id))
    flash("Migrace provedena úspěšně. Prosíme ověřte datové sady ve svém lokálním katalogu.", 'success')
    return redirect(url_for('site.index'))


@site.route('/authors', methods=['GET'])
def authors():
    return render_template(template_path + 'contact.html')


@site.route('/logout', methods=['GET'])
def logout():
    user = LKODUser('')
    user.logout()
    return redirect(url_for('site.index'))


@site.route('/validate/<dataset>', methods=['GET'])
def validate(dataset):
    if 'user' not in session and 'accessToken' not in session['user']:
        flash('Please log in again to migrate data', 'warning')
        redirect(url_for('site.index'))

    if 'lkod' not in session and 'ckan' not in session:
        flash('Please reinsert data into form, to be able to submit data', 'warning')
        redirect(url_for('site.index'))

    lkod_configuration = session['lkod']
    ckan_configuration = session['ckan']
    organization_configuration = session['user']
    migrator_cls = Migrator(lkod_configuration['url'], ckan_configuration['url'], ckan_configuration['api_key'],
                            organization_configuration['vatin'])
    nkod_dataset = migrator_cls.get_transformed_dataset(dataset)
    nkod_dataset.validate()
    return nkod_dataset.errors if len(nkod_dataset.errors) else 'Nenalezeny žádné chyby v datové sadě'


@site.route('/installation')
def installation():
    return render_template(template_path + 'installation.html')
