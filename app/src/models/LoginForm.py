from typing import NoReturn
import os

from flask import session
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, URL, Length, ValidationError
import requests
from app.src.models.LKODUser import LKODUser
from app.src.models.Migrator import Migrator
from app.src.components.clm.MigrationVariant import MigrationVariant
from app.src.models.Migration import STATUS_INIT, STATUS_READY


class LoginForm(FlaskForm):
    user = None
    migrator = None
    datasets = []
    status = STATUS_INIT
    ckan_server_url = StringField('URL CKAN API serveru', validators=[DataRequired(), URL()])
    ckan_api_key = StringField('API klíč CKAN', validators=[Length(0, 50)])
    ckan_organization_name = StringField('Název organizace v CKAN (zkratka v API)', validators=[])
    lkod_server_url = StringField('URL LKOD API serveru', validators=[DataRequired(), URL()])
    company_vatin = StringField('IČO', validators=[DataRequired()])
    lkod_company_id = StringField('ID', validators=[])
    username = StringField('LKOD Už. jméno', validators=[DataRequired()])
    password = PasswordField('LKOD Heslo', validators=[DataRequired()])
    login_form_submit = SubmitField('Načíst datové sady')

    def validate_ckan_server_url(self, field) -> NoReturn:
        if self.check_server(field.data.strip('/')) is False:
            raise ValidationError("CKAN API server není dostupný. Prosím ověřte adresu serveru, případně kontaktujte provozovatele LKOD serveru.")
        if self.login_CKAN() is False:
            raise ValidationError("Přihlášení k lokálnímu katalogu CKAN nebylo úspěšné.")

    def validate_lkod_server_url(self, field) -> NoReturn:
        if self.check_server(field.data.strip('/')) is False:
            raise ValidationError("LKOD server není dostupný. Prosím ověřte adresu serveru, případně kontaktujte provozovatele LKOD serveru.")
        if self.check_server('/'.join([field.data.strip('/'), 'health'])) is False:
            raise ValidationError("LKOD API není dostupné. Prosím ověřte adresu serveru, případně kontaktujte provozovatele LKOD serveru.")

    def validate_company_vatin(self, field) -> NoReturn:
        if self.login_LKOD() is False:
            raise ValidationError("Instituce nebyla v LKOD nalezena! Vyplňte prosím ID organizace ")

        if len(self.lkod_company_id.data) and self.user.set_organization(organization_id=self.lkod_company_id.data) is False:
            raise ValidationError("Instituce nebyla v LKOD nalezena! Vyplňte prosím ID organizace.")

        if self.user.set_organization(vatin=field.data) is False:
            raise ValidationError("Instituce nebyla v LKOD nalezena! Vyplňte prosím ID organizace.")

    def validate_username(self, field) -> NoReturn:
        if self.login_LKOD() is False:
            raise ValidationError("Přihlášení k lokálnímu katalogu LKOD nebylo úspěšné.")

    def process_data(self) -> bool:
        session['lkod'].update({'url': self.lkod_server_url.data.strip('/')})
        ckan_params = {'url': self.ckan_server_url.data.strip('/'), 'api_key': self.ckan_api_key.data}
        if len(self.ckan_organization_name.data):
            ckan_params.update({'organization': self.ckan_organization_name.data})
        session['ckan'] = ckan_params

        self.migrator = Migrator(self.lkod_server_url.data.strip('/'), self.ckan_server_url.data.strip('/'), self.ckan_api_key.data, self.company_vatin.data)
        self.user.set_organization(self.company_vatin.data, self.lkod_company_id.data)
        self.datasets = self.migrator.prepare_datasets()
        settings = {'datasets': self.datasets}
        if os.getenv('DEBUG'):
            print(" DEBUG: SETTINGS: ", settings)
        if 'datasets' in session: 
            session['datasets'].update(settings)
        else:
            session['datasets'] = settings
        self.status = STATUS_READY
        return True

    def login_CKAN(self) -> bool:
        return True

    def login_LKOD(self) -> bool:
        try:
            self.user = LKODUser('')
            self.user = LKODUser(self.lkod_server_url.data.strip('/'))
            status = self.user.login(self.username.data, self.password.data)
            return status
        except requests.exceptions.RequestException as e:
            return False

    def check_server(self, url) -> bool:
        try:
            requests.get(url, timeout=3)
            return True
        except requests.exceptions.RequestException as e:
            return False

    def is_ready(self) -> bool:
        return self.status == STATUS_READY
