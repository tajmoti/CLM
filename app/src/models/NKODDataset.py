from distutils.command.upload import upload
import os
import json
import requests
import string
import random
import shutil

from datetime import datetime
from datetime import date
from urllib.parse import urljoin, urlparse
from functools import reduce

from app.src.models.CKANDataset import CKANDataset
from flask import flash
from typing import Union

from app.src.components.clm.JSONValidator import JSONValidator

FREQUENCY_MAP = {'R/P10Y': 'DECENNIAL', 'R/P4Y': 'QUADRENNIAL', 'R/P3Y': 'TRIENNIAL', 'R/P2Y': 'BIENNIAL',
                 'R/P1Y': 'ANNUAL', 'R/P6M': 'ANNUAL_2', 'R/P4M': 'ANNUAL_3', 'R/P3M': 'QUARTERLY',
                 'R/P2M': 'BIMONTHLY', 'R/P0.5M': 'BIMONTHLY', 'R/P1M': 'MONTHLY', 'R/P0.33M': 'MONTHLY_3',
                 'R/P1W': 'WEEKLY', 'R/P3.5D': 'WEEKLY_2', 'R/P0.33W': 'WEEKLY_3', 'R/P2W': 'BIWEEKLY',
                 'R/P0.5W': 'BIWEEKLY', 'R/P1D': 'DAILY', 'R/PT1H': 'HOURLY', 'R/PT1S': 'UPDATE_CONT'}

RUIAN_MAP = {
    'st': 'stat',
    'mč': 'mestskecasti',
    'ob': 'obec',
    'ok': 'okres',
    'vúsc': 'vusc',
    'kr': ''
    # 'kraj-1960', 'cast-obce', 'momc', 'mop', 'orp', 'parcela', 'pou', 'region-soudruznosti', 'zsj', 'spravni-obvod', 'ulice', 'volebni-okrsek',
}

class NKODDataset:
    json_validator = None
    lkod_url = ''
    errors = []

    dataset = None
    dataset_obj = None

    context = 'https://ofn.gov.cz/rozhraní-katalogů-otevřených-dat/2021-01-11/kontexty/rozhraní-katalogů-otevřených-dat.jsonld'
    type = 'Datová sada'
    name = ''
    description = ''
    provider = None
    theme = ["http://publications.europa.eu/resource/authority/data-theme/OP_DATPRO"]
    periodicity = None
    keyword = {}
    ruian = None
    contact_point = {}
    distribution = []
    eurovoc = []
    documentation = None
    timespan = None
    resources = []

    def __init__(self, lkod_url, vatin, dataset: CKANDataset, form:dict = None) -> None:
        self.resources = []
        self.distribution = []
        self.poskytovatel = '/'.join(['https://rpp-opendata.egon.gov.cz/odrpp/zdroj/orgán-veřejné-moci/', vatin]),
        self.lkod_url = lkod_url
        self.dataset = dataset.name
        self.dataset_obj = dataset
        dataset_data = dataset.data
        if ('maintainer' in dataset_data and len(dataset_data['maintainer'])) or ('maintainer_email' in dataset_data and len(dataset_data['maintainer_email'])):
            organization = {'typ': 'Organizace'}
            if 'maintainer' in dataset_data and len(dataset_data['maintainer']):
                organization.update({'jméno': {'cs': dataset_data['maintainer']}})

            if 'maintainer_email' in dataset_data and len(dataset_data['maintainer_email']):
                organization.update({'e-mail': ''.join(['mailto:', dataset_data['maintainer_email']])})
            self.contact_point = organization

        if len(dataset_data['title']):
            self.name = {'cs': dataset_data['title']}

        if dataset_data['notes'] is not None and len(dataset_data['notes']):
            self.description = {'cs': dataset_data['notes']}

        if 'frequency' in dataset_data and self.convert_ISO_8601_to_eu_frequency(dataset_data['frequency']) is not None:
            self.periodicity = self.convert_ISO_8601_to_eu_frequency(dataset_data['frequency'])

        if self.periodicity is None and form is not None and form['prefill_frequency_check']:
            self.periodicity = '/'.join(
                ['http://publications.europa.eu/resource/authority/frequency', form['prefill_frequency'].upper()])
        elif self.periodicity is None and form is not None:
            self.periodicity = 'http://publications.europa.eu/resource/authority/frequency/IRREG'

        if 'spatial_uri' in dataset_data and len(dataset_data['spatial_uri']):
            ruian = self.get_ruian_type(uri=dataset_data['spatial_uri'])
            if ruian != None and ruian != '':
                self.ruian = [ruian]
        elif 'ruian_type' in dataset_data and 'ruian_code' in dataset_data:
            ruian = self.get_ruian_type(ruian_type=dataset_data['ruian_type'], ruian_code=dataset_data['ruian_code'])
            if ruian != None and ruian != '':
                self.ruian = [ruian]

        if self.ruian is None and form is not None and form['prefill_ruian_check']:
            self.ruian = ['https://linked.cuzk.cz/resource/ruian/stat/1']

        self.provider = '/'.join(['https://linked.opendata.cz/zdroj/ekonomický-subjekt', vatin])

        if 'theme' in dataset_data and len(dataset_data['theme']):
            eurovoc = dataset_data['theme'].split('/')[-1]
            self.eurovoc = ['/'.join(['http://eurovoc.europa.eu', eurovoc])]

        if 'schema' in dataset_data and len(dataset_data['schema']):
            self.documentation = dataset_data['schema']
        else:
            self.documentation = ''

        if 'extras' in dataset_data:
            extras = dataset_data['extras']
            theme = [element for element in extras if element['key'] == 'theme']
            if len(theme) > 0:
                self.theme = theme[0]['value']

        if 'temporal_start' in dataset_data or 'temporal_end' in dataset_data:
            self.timespan = {
                'typ': 'Časový interval',   
            }
            if 'temporal_start' in dataset_data:
                self.timespan.update({'začátek': str(dataset_data['temporal_start']),})
            
            if 'temporal_end' in dataset_data:
                temporal_end = str(dataset_data['temporal_end']) if len(str(dataset_data['temporal_end'])) > 0 else str(date.today())
                self.timespan.update({'konec': temporal_end,})

        if 'tags' in dataset_data:
            tags = dataset_data['tags']
            new_tags = {'cs': []}
            for tag in tags:
                new_tags['cs'].append(tag['display_name'])
            self.keyword = new_tags

        if len(dataset_data['resources']) == 0:
            return

        for resource in dataset_data['resources']:
            new_resource = {
                'typ': 'Distribuce',
                'podmínky_užití': self.get_license_prefill_prefill_value(dataset_data, resource, form),
                'název': {
                    'cs': resource['name']
                },
                'soubor_ke_stažení': resource['url'],
                'přístupové_url': resource['url'],
                'formát': '/'.join(['http://publications.europa.eu/resource/authority/file-type', resource['format']]),
                'extension': resource['format']
            }
            if 'mimetype' in resource and resource['mimetype'] is not None:
                new_resource['typ_média'] = '/'.join(['http://www.iana.org/assignments/media-types', resource[
                    'mimetype']])
            self.resources.append(new_resource)

    def convert_ISO_8601_to_eu_frequency(self, value) -> Union[str, None]:
        if value in FREQUENCY_MAP:
            return '/'.join(['http://publications.europa.eu/resource/authority/frequency', FREQUENCY_MAP[value]])
        return None

    def get_final_format(self, mimetype) -> str:
        if mimetype is None:
            return ''
        m = mimetype.split('/')
        if len(m) == 2:
            return m[1]
        return ''

    def get_ruian_type(self, uri=None, ruian_type=None, ruian_code=None) -> str:
        if uri is not None:
            uri_params = uri.split('/')[-2:]
            if len(uri_params) == 2:
                return '/'.join(['https://linked.cuzk.cz/resource/ruian', uri_params[0], uri_params[1]])
            else:
                return ''

        if (ruian_type is not None and ruian_code is None) or ruian_code == '' or ruian_type == '':
            return ''

        if ruian_type is not None:
            ruian = str(ruian_type).lower()
            if ruian in RUIAN_MAP:
                ruian = RUIAN_MAP[ruian]
            return '/'.join(['https://linked.cuzk.cz/resource/ruian', ruian, ruian_code])
        return ''

    def get_license_prefill_prefill_value(self, dataset_data, resource, form=None) -> dict:
        license_obj = {'typ': 'Specifikace podmínek užití', 'autorské_dílo': '', 'osobní_údaje': 'https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/', 'autorské_dilo': 'https://data.gov.cz/podmínky-užití/neobsahuje-autorská-díla/'}
        if 'license_link' in resource and len(resource['license_link']):
            license_obj['autorské_dílo'] = resource['license_link']

        if license_obj['autorské_dílo'] is None and 'license_url' in dataset_data and len(dataset_data['license_url']):
            license_obj['autorské_dílo'] = dataset_data['license_url']

        license_obj['databáze_jako_autorské_dílo'] = dataset_data['license_url'] if 'license_url' in dataset_data else None
        license_obj['databáze_chráněná_zvláštními_právy'] = 'https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/'

        if license_obj['autorské_dílo'] is None and form is not None and form['prefill_license_check']:
            if form['prefill_license'] == 'cc4':
                license_obj['autorské_dílo'] = 'https://creativecommons.org/licenses/by/4.0'
            elif form['prefill_license'] == 'none':
                license_obj['autorské_dílo'] = 'https://data.gov.cz/podmínky-užití/neobsahuje-autorská-díla/'

        if license_obj['databáze_jako_autorské_dílo'] is None and form is not None and form['prefill_license_check']:
            if form['prefill_license'] == 'none':
                license_obj['databáze_jako_autorské_dílo'] = 'https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/'

        return license_obj

    def create(self, base_url: str, user_configuration: dict, organization_configuration: dict) -> dict:
        response = requests.post('/'.join([base_url, 'datasets']),
                             data={'organizationId': organization_configuration['organizationId']},
                             headers={
                                 'Authorization': 'Bearer ' + user_configuration['accessToken']}).json()

        if 'id' not in response:
            raise requests.ConnectionError('Dataset creation error')
        self.upload_files(base_url, user_configuration, response['id'])
        return response

    def update(self, base_url: str, user_configuration: dict, session_id: str, dataset_id: str) -> bool:

        user_data = {'accessToken': user_configuration['accessToken'], 'sessionId': session_id,
                     'datasetId': dataset_id}
        try:
            response = requests.post('/'.join([base_url, 'form-data']),
                          headers={'ContentType': 'application/x-www-form-urlencoded'},
                          json={'userData': json.dumps(user_data), 'formData': self.json()})
        
            print(" STATUS: ", response.ok)
            if (response.ok):
                print(" RESPONSE: ", response.content)
        except requests.ConnectionError:
            return False
        return True

    def upload_files(self, base_url: str, user_configuration: dict, dataset_id: str) -> bool:
        root = os.path.dirname(os.path.abspath(__file__))
        files_dir = os.path.join(root, 'migration', ''.join(random.choices(string.ascii_uppercase +
                                                                           string.digits, k=10)))
        os.makedirs(files_dir)
        try:
            for i in range(len(self.resources)):
                resource = self.resources[i]
                file_url = resource["soubor_ke_stažení"]
                if len(self.dataset_obj.fetcher.config.base_url):
                    parsed = urlparse(self.dataset_obj.fetcher.config.base_url)
                    if file_url.find(parsed.netloc) == -1:
                        print(" DEBUG: FILE_URL: ", file_url, "external file, wont process")
                        self.resources[i] = resource
                        continue
                print(" DEBUG: FILE_URL: ", file_url, "internal file, processing")
                filename = os.path.basename(urlparse(file_url).path)
                file = requests.get(file_url, stream=True)

                if len(filename):
                    with open(os.path.join(files_dir, filename), 'wb') as f:
                        for chunk in file.iter_content(chunk_size=1024):
                            if chunk:
                                f.write(chunk)

                    print(" DEBUG: FILE_PATH: ", os.path.join(files_dir, filename))
                    upload_request = requests.post('/'.join([base_url, 'datasets', dataset_id, 'files']),
                                                headers={'ContentType': 'multipart/form-data',
                                                        'Authorization': 'Bearer ' + user_configuration['accessToken']},
                                                files={'datasetFile': open(os.path.join(files_dir, filename), 'rb')})
                    print(" DEBUG: UPLOAD_REQUEST_CONTENT: ", upload_request.content)
                    if upload_request.ok:
                        if os.getenv('DEBUG'):
                            print(" DEBUG: UPLOAD_REQUEST: ", upload_request)
                        upload_request = upload_request.json()
                        resource["soubor_ke_stažení"] = resource["přístupové_url"] = upload_request['file']                        
                        self.resources[i] = resource
                    else:
                        flash("Migrace na uložiště nebyla úšpěšně provedena. Vypadá to, že uložiště LKOD není dostupné. Prosím zkontrolujte uložiště a zkuste migraci znovu.", 'error')
            self.distribution = self.resources
            print(" DEBUG: DISTRIBUTION: ", self.distribution)

        except requests.ConnectionError:
            shutil.rmtree(files_dir, ignore_errors=True)
            print(" DEBUG: upload error")
            return False
        shutil.rmtree(files_dir, ignore_errors=True)
        return True

    def validate(self) -> bool:
        validator = JSONValidator(self.lkod_url)
        status = validator.validate_json(self.json())
        self.errors = validator.errors
        return status

    def json(self) -> str:
        return json.dumps({
            "@context": self.context,
            "typ": self.type,
            "název": self.name,
            "popis": self.description,
            "téma": self.theme,
            "poskytovatel": self.provider,
            "klíčové_slovo": self.keyword,
            "prvek_rúian": self.ruian,
            "kontaktní_bod": self.contact_point,
            "distribuce": self.distribution,
            "koncept_euroVoc": self.eurovoc,
            "dokumentace": self.documentation,
            "časové_pokrytí": self.timespan,
            "periodicita_aktualizace": self.periodicity
        })

    def __repr__(self) -> str:
        return self.json()
