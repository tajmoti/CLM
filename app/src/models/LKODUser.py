from typing import Optional
import os

import requests
import json
from flask import session


class LKODUser:
    url = None
    vatin = None
    access_token = None
    organization_id = None

    organizations = []

    def __init__(self, url) -> None:
        self.url = url

    def login(self, username, password) -> bool:
        data = requests.post('/'.join([self.url, 'auth/login']), data={'email': username, 'password': password})
        if os.getenv('DEBUG'):
            print(" DEBUG: URL: ", self.url)
            print(" DEBUG: LOGIN: ", username)
            print(" DEBUG: PASSWORD: ", password)
            print(" DEBUG: LOGIN RESPONSE: ", data.content)
        if not data.ok:
            return False
        content = json.loads(data.content)
        if 'accessToken' not in content:
            self.logout()
            return False

        user = content
        self.access_token = user['accessToken']
        if 'user' not in session or session['user'] is None:
            session['user'] = user
        else:
            session['user'].update(user)
        session['lkod'] = {'url': self.url}
        return True

    def set_organization(self, vatin: Optional[str] = None, organization_id: Optional[int] = None) -> bool:
        self.organizations = requests.get('/'.join([self.url, 'organizations']),
                            headers={'Authorization': 'Bearer ' + self.access_token}).json()
        if os.getenv('DEBUG'):
            print(" DEBUG: ORGANIZATIONS: ", self.organizations)
        for organization in self.organizations:
            if organization_id is not None and organization_id == str(organization['id']):
                self.set_organization_id(organization['id'])
                self.set_organization_vatin(organization['identificationNumber'])
                return True

        for organization in self.organizations:
            if vatin is not None and vatin == organization['identificationNumber']:
                self.set_organization_id(organization['id'])
                self.set_organization_vatin(organization['identificationNumber'])
                return True
        return False

    def logout(self) -> None:
        self.url = None
        if 'user' in session:
            del session['user']
        if 'settings' in session:
            del session['settings']
        if 'ckan' in session:
            del session['ckan']
        if 'lkod' in session:
            del session['lkod']
        if 'migration' in session:
            del session['migration'] 
        if 'datasets' in session:
            del session['datasets'] 
        

    def set_organization_vatin(self, vatin: str) -> None:
        self.vatin = vatin
        organization = self.get_organization_from_session()
        user = session['user'] if 'user' in session else {}
        organization['vatin'] = self.vatin
        user.update(organization)
        session['user'].update(user)

    def set_organization_id(self, organization_id: int) -> None:
        self.organization_id = organization_id
        organization = self.get_organization_from_session()
        user = session['user'] if 'user' in session else {}
        organization['organizationId'] = self.organization_id
        user.update(organization)
        session['user'].update(user)

    def get_organization_from_session(self) -> dict:
        return session['organization'] if 'session' in session else {}
